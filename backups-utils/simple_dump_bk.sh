#!/bin/bash
# Respaldar base de datos de MySQL con
# mysqldump y archivo de configuración
# usando cron para hacerlo de manera periódica
# 
# https://parzibyte.me/blog/2019/02/05/instalar-configurar-mysql-linux-ubuntu/
# https://parzibyte.me/blog/2019/02/06/script-respaldar-base-de-datos-mysql-mariadb-linux-mysqldump/
# https://parzibyte.me/blog/2018/07/02/linux-script-respaldar-carpeta-bash-tar/
# 
# @author parzibyte | Visita: parzibyte.me/blog

# Bucket S3
BUCKET_S3=devti

# El nombre de tu base de datos de MySQL/MariaDB
NOMBRE_BASE_DE_DATOS="wp_casamami_prod"
FECHA=$(date "+%d-%m-%y_%H-%M-%S")
# Ruta absoluta en donde está tu usuario y contraseña, mira el segundo link de arriba
RUTA_ARCHIVO_CONFIGURACION="/home/backups/my.cnf"
# Ruta absoluta de la carpeta en donde se van a crear los respaldos
RUTA_SALIDA_RESPALDO="/home/backups/"
# Da algo como respaldo_pruebas_06-02-2019_09-22-20.sql
NOMBRE_ARCHIVO_RESPALDO="bk_${NOMBRE_BASE_DE_DATOS}_${FECHA}.sql"
# Simple concatenación
RUTA_ARCHIVO_RESPALDO="$RUTA_SALIDA_RESPALDO/$NOMBRE_ARCHIVO_RESPALDO"
# Y llamamos al comando de mysqldump, guardamos la salida en el respaldo y ya está :)
echo "[+] Creating Database dump..."
mysqldump --defaults-file=$RUTA_ARCHIVO_CONFIGURACION $NOMBRE_BASE_DE_DATOS > $RUTA_ARCHIVO_RESPALDO

echo "[+] bzip2 running database file..."
tar cf casamami_database.tgz -j $RUTA_ARCHIVO_RESPALDO

# RENAMING TAR FILE
BACKUP_FILE_NAME=casamami_db_wp_backup_
CURRENT_DATE=$(date +"%Y%m%d")
DELETE_DATE=$(date --date="3 days ago" +"%Y%m%d")
DELETE_FILE_NAME=${BACKUP_FILE_NAME}${DELETE_DATE}.tgz
NEW_TAR_NAME=${BACKUP_FILE_NAME}${CURRENT_DATE}.tgz
mv casamami_database.tgz ${NEW_TAR_NAME}

# UPLOADING TAR TO AWS S3
echo "[+] Uploading tar to S3..."
s3cmd --storage-class=STANDARD_IA put ${NEW_TAR_NAME} s3://${BUCKET_S3}/
# REMOVING LOCAL FILES
echo "[+] Removing local files..."
rm -rf ${RUTA_ARCHIVO_RESPALDO} ${NEW_TAR_NAME}
s3cmd rm s3://${BUCKET_S3}/${DELETE_FILE_NAME}
echo "[+] Finish!"
echo ""