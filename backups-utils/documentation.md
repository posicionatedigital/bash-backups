# Instalar y configurar 3cmd
sudo apt-get install s3cmd

## Configurar s3cmd
s3cmd --configure

# Crear archivo ejecutable
touch backup_wp.sh && chmod +x backup_wp.sh && nano backup_wp.sh

## Cambiar las variables
WP_DIR=/path/to/wordpress-dir
BUCKET_S3=your-s3-bucket-name
NEW_TAR_NAME=wp_backup_${CURRENT_DATE}.tgz

## Crontab 
crontab -e 
0 1 * * /path/to/backup_wp.sh >/dev/null 2>&1


# Restaurar Backup
Creamos ejecutable
touch restore_wp.sh && chmod +x restore_wp.sh && nano restore_wp.sh

Ejecutamos
./restore_wp.sh



https://medium.com/the-i/how-to-automate-wordpress-backup-with-linux-and-aws-a13cb167b09e